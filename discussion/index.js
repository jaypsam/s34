// EXPRESS SETUP


//4 Running
//1. Import by using the 'require' directive to get access to the components of express package/ dependency
const express = require('express')

//2. Use the express() function and assign in to an app variable to create an express app or app server
const app = express()

//3. Declare a variable for the port of the server
const port = 3000

//+++++++++++++++++++++++++++++++++++++++++++++++++++
// Middlewares
// These two .use are essential in express
// Allows your app to reas json format data
app.use(express.json())

// Allows your app to read data from forms:
app.use(express.urlencoded({extended: true}))

//+++++++++++++++++++++++++++++++++++++++++++++++++++
// Routes
app.get('/homepage', (request, response) => {
	response.send('Welcome to the homepage')
})

app.get('/users', (request, response) => {
	console.log(users)
	response.send(users)
})

//+++++++++++++++++++++++++++++++++++++++++++++++++++
// Register user route

//mock database
let users = [];

//Activity

app.post('/register', (request, response) => {
	if(request.body.username !== " " && request.body.password !== " "){
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} succesfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Put request route
app.put('/change-password', (request, response) => {
	let message 

	for(let i = 0; i < users.length; i++){
		if (request.body.username == users[i].username){
			users[i].password == request.body.password
			message = `User ${request.body.username}'s has been updated`
			break
		} else {
			message = 'User does not exist.'
		}
	}
	response.send(message)
})

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

app.delete('/delete-user',(request,response) => {

  let message;

  for(let i = 0; i<=users.length; i++){
    if(request.body.username == users[i].username && request.body.password == users[i].password ){
        users.splice(i,1)
        message = `User ${request.body.username}'s has been deleted`
        break;
    }else { message = 'User does not exist'}
  }
  response.send(message)

})






app.listen(port, () => console.log(`Server is running at port ${port}`))